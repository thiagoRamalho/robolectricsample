package br.com.trama.robolectricsample;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import br.com.trama.robolectricsample.config.ShadowSupportMenuInflater;

/**
 * Created by thiago on 14/03/15.
 *
 * workaround para trabalhar sem criar o customRunner
 * porem o shadow deve ser criado
 */
// MANIFEST PELA IDE DEVE UTILIZAR ESSA ATRIBUICAO PARA O MANIFEST
//CASO EXECUTE PELO COMANDO (./gradlew) adicionar app/ ao caminho do mesmo
@Config(manifest = "app/src/main/AndroidManifest.xml", emulateSdk = 18, shadows = {ShadowSupportMenuInflater.class})
@RunWith(RobolectricTestRunner.class)
public class RobolectricAnnotationTest extends AbstractRobolectricTemplate{

    @Test
    public void deveExecutarSemErros() {
        super.deveExecutarSemErros();
    }
}
