package br.com.trama.robolectricsample;

import android.widget.TextView;

import org.robolectric.Robolectric;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Created by thiago on 13/03/15.
 */
public class AbstractRobolectricTemplate {

    public void deveExecutarSemErros() {
        MainActivity activity = Robolectric.setupActivity(MainActivity.class);

        TextView results =
                (TextView) activity.findViewById(R.id.textview);
        String resultsText = results.getText().toString();

        // failing test gives much better feedback
        // to show that all works correctly ;)
        assertThat(resultsText, equalTo(activity.getString(R.string.hello_world)));
    }
}
