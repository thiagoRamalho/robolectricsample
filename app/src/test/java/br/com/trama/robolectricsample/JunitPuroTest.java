package br.com.trama.robolectricsample;

import org.junit.Test;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * Teste com a finalidade de testar o uso do JUnit sem
 * o robolectric
 *
 */
public class JunitPuroTest {

    public static final boolean VALUE = true;

    @Test
    public void deveExecutarSemErros() {
        assertThat(true, is(VALUE));
    }


}